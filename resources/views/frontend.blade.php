<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Creative Artist Group</title>

    <link href="{{ asset('public/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/font-awesome.css') }}" rel="stylesheet">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,600,800%7COpen+Sans:400italic,400,600,700'
          rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('public/js/html5.js') }}"></script>
    <script src="{{ asset('public/js/respond.min.js') }}"></script>
    <![endif]-->
</head>

<body>
<div class="top-bar">

    <div class="right-links">

        @if(!isset($user))

            <a href="{{url('auth/login')}}"> Log in </a> | <a href="{{url('auth/register')}}">Register</a>
        @else
            Welcome {!! $user->name !!}| <a href="{{url('/dashboard')}}"> Dashboard </a> | <a href="{{url('auth/logout')}}"> Logout </a>
        @endif

    </div>
</div>
{{--navigation section--}}
@include('includes/frontend/nav')

@yield('content')

<!-- Footer -->
<footer>
    <div class="wrapper row">
        <div class="fl_left col-md-4">Copyright &copy; 2015 - All Rights Reserved - <a href="#">Artist Forum</a>
        </div>
        <div class="fl_center col-md-4">
            <ul class="list-inline ">
                <li><a href="#"><i class="fa fa-facebook-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square fa-2x"></i></a></li>
            </ul>
        </div>
        <div class="fl_right col-md-4">Maintained By: Robust IT Concepts Pvt. Ltd.</div>
        <br class="clear"/>
    </div>
</footer>

<script src="{{ asset('public/js/all.js') }}"></script>
</body>
</html>
