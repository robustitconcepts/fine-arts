@extends('admin')

@section('breadcrumb')
    <div class = "portfolio-content-header">
        <h3 class="panel-title">Add Portfolio</h3>
    </div>
@stop

@section('adminContent')
    <div class = "portfolio-content">

                        {!! Form::open(['route'=>'portfolios.store','files' => true])!!}
                        <fieldset>
                              <div class = "col-md-6">

                                 <div class="form-group">
                                     <label><b>FIRST NAME</b></label>
                                     {!! Form::text('first_name',null,array('class'=>'form-control')) !!}
                                 </div>
                                 <div class="form-group">
                                     <label><b>LAST NAME</b></label>
                                     {!! Form::text('last_name',null,array('class'=>'form-control')) !!}
                                 </div>                              

                                  <div class="form-group">
                                       <label>SPECIALISATION</label>
                                      {!! Form::select('art_category_id',$arts,null,array('class'=>'form-control')) !!}
                                   </div>
                                  <div class="form-group">
                                       <label>TAGS</label><br>
                                       {!! Form::select('tags[]',$tags,null,['id'=>'tag_id','class'=>'form-control','multiple']) !!}
                                  </div>
                               </div>
                                 <div class = "col-md-6">
                                   <div class="form-group">
                                         <label><b>CONTACT NO.</b></label>
                                         {!! Form::text('contact_no',null,array('class'=>'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                          <label><b>DESCRIPTION</b></label>
                                          {!! Form::textarea('description',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                         <label><b>USERS</b></label>
                                         {!! Form::select('user_id', $userWithoutProfile, null, ['class'=>'form-control'])!!}
                                    </div>
                                </div>

                                <center>{!! form::submit('Add',[' class'=>'btn btn-primary form-control'])!!}</center>

                            </fieldset>
                        {!! Form::close()!!}

                    </div>


@stop
@section('js_code')
    <script>
       $(function(){
            console.log("testing");
            $('#tag_id').select2({
                placeholder:'choose a tag',
                tags:true
            });

        });
    </script>
   <link href="{{asset('public/select/css/select2.min.css')}}" rel="stylesheet">
<script src="{{ asset('public/select/js/select2.min.js') }}"></script>
@endsection