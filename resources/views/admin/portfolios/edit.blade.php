@extends('admin')

@section('breadcrumb')
    <div class="portfolio-content-header">
        <h3 class="panel-title">Edit Profile</h3>
    </div>
@stop

@section('adminContent')
    <div class="portfolio-content">

        {!! Form::model($portfolio,array('method' => 'PATCH','route'=>array('portfolios.update',$portfolio->id))) !!}


        <div class="col-md-6">
            <div class="form-group">
                <label><b>FIRST NAME</b></label>
                {!! Form::text('first_name',null,array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                <label><b>LAST NAME</b></label>
                {!! Form::text('last_name',null,array('class'=>'form-control')) !!}
            </div>

           
            <div class="form-group">
                <label>SPECIALIZATION</label>
                {!! Form::select('art_category_id',$arts, null,array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                <label>TAGS</label><br>
                {!! Form::select('tag_list[]',$tags,null,array('id'=>'tag_id','class'=>'form-control','multiple')) !!}
            </div>
            <div class="form-group">
                <label><b>CONTACT NO.</b></label>
                {!! Form::text('contact_no',null,array('class'=>'form-control')) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label><b>DESCRIPTION</b></label>
                {!! Form::textarea('description',null,array('class'=>'form-control')) !!}
            </div>
        </div>
         {!! Form::hidden('user_id') !!}

        <center>{!! form::submit('Update',[' class'=>'btn btn-primary form-control'])!!}</center>


        {!! Form::close()!!}

    </div>

@stop

