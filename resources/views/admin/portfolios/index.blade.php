@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Users</h3>
    <div class="sub-menu">
        <a href="{{url('portfolios/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>
    </div>
@stop

@section('adminContent')
    <div class="table-responsive portfolio-content">
        <table class="table table-hover table-striped">
            <thead>
            <th> FIRSTNAME</th>
            <th> LASTNAME</th>
            <th> ART CATEGORY</th>
            <th> CONTACT</th>
            <th> STATUS</th>
            <th> ACTIONS</th>
            </thead>
            <tbody>

                @foreach($portfolios as $portfolio)
                   <tr>
                    <td>{!! $portfolio->first_name !!}</td>
                    <td>{!! $portfolio->last_name !!}</td>
                    <td>{!! $portfolio->art_category->name!!}</td>
                    <td>{!! $portfolio->contact_no !!}</td>
                    <td><!-- Single button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                {!! $portfolio->user->status !!} <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{!! url('userstatus/'. $portfolio->user->id . '/Active') !!}">Active</a></li>
                                <li><a href="{!! url('userstatus/'. $portfolio->user->id . '/Banned') !!}">Banned</a></li>
                                <li><a href="{!! url('userstatus/'. $portfolio->user->id . '/On Hold') !!}">On Hold</a></li>
                            </ul>
                        </div></td>

                     <td>{!! link_to_route('portfolios.edit', '', array($portfolio->id),array('class' => 'fa fa-pencil fa-fw')) !!}
                       </td>

                @endforeach
            </tr>
            </tbody>
        </table>
        <div class="pagination"> {!! $portfolios->render() !!}</div>
    </div>
@stop
