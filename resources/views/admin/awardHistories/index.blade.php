@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Award History</h3>

    <div class="sub-menu">
        <a href="{{url('awardhistories/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>

    </div>
@stop

@section('adminContent')

    <div class="table-responsive award-content">
        <table class="table table-hover table-striped">
            <thead>
            <th> TITLE</th>
            <th> YEAR</th>
            <th> NAME</th>
            <th> ACTIONS</th>
            </thead>

            <tbody>

                @foreach($awardhistories as $awardhistory)
                    <tr>
                    <td>{!! $awardhistory->title !!}</td>
                    <td>{!! $awardhistory->year !!}</td>
                    <td>{!! $awardhistory->portfolio->all()[0]->first_name!!}</td>
                    <td>{!! link_to_route('awardhistories.edit', '', array($awardhistory->id),array('class' => 'fa fa-pencil fa-fw')) !!}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
        <div class="pagination"> {!! $awardhistories->render() !!} </div>
    </div>
@stop
