@extends('admin')

@section('breadcrumb')
    <div class = "portfolio-content-header">
        <h3 class="panel-title">Edit Award History</h3>
    </div>
@stop

@section('adminContent')
    <div class = "portfolio-content">
                        {!! Form::model($awardhistories, array('method' =>'PATCH','route'=>array('awardhistories.update', $awardhistories->id))) !!}

                        <fieldset>
                            <div class="form-group">
                                <label><b>TITLE</b></label>
                                {!! Form::text('title',null,array('class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label><b>YEAR</b></label>
                                {!! Form::text('year',null,array('class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                              <label>USER</label>
                              {!! Form::select('portfolio_id', $portfolioLists) !!}
                            </div>
                            {!! form::submit('Update',[' class'=>'btn btn-primary form-control'])!!}

                        </fieldset>
                        {!! Form::close()!!}

                    </div>

@stop

