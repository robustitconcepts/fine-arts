@extends('admin')

@section('breadcrumb')
    <div class = "portfolio-content-header">
        <h3 class="panel-title">Add Award</h3>
    </div>
@stop

@section('adminContent')
    <div class = "article-content">

                        {!! Form::open(['route'=>'awardhistories.store'])!!}

                            <fieldset>
                                 <div class="form-group">
                                     <label><b>TITLE</b></label>
                                     {!! Form::text('title',null,array('class'=>'form-control')) !!}
                                 </div>
                                 <div class="form-group">
                                     <label><b>YEAR</b></label>
                                     {!! Form::text('year',null,array('class'=>'form-control')) !!}
                                 </div>
                                <div class="form-group">
                                  <label>NAME</label>
                                  {!! Form::select('portfolio_id', $portfolioLists) !!}
                                </div>
                                <center>{!! form::submit('Add',[' class'=>'btn btn-primary form-control'])!!}</center>

                            </fieldset>
                        {!! Form::close()!!}

                    </div>

@stop

