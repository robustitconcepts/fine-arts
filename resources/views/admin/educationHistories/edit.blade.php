@extends('admin')

@section('breadcrumb')
    <div class = "education-content-header">
        <h3 class="panel-title">Edit Education</h3>
    </div>
@stop

@section('adminContent')
    <div class = "education-content">

                        {!! Form::model($educationHistory,array('method' => 'PATCH','route'=>array('education.update',$educationHistory->id))) !!}

                            <fieldset>
                                 <div class="form-group">
                                     <label><b>SCHOOL</b></label>
                                     {!! Form::text('school',null,array('class'=>'form-control')) !!}
                                 </div>
                                 <div class="form-group">
                                     <label><b>START DATE</b></label>
                                     {!! Form::text('start_date',null,array('class'=>'form-control')) !!}
                                 </div>
                                 <div class="form-group">
                                     <label><b>END DATE</b></label>
                                     {!! Form::text('end_date',null,array('class'=>'form-control')) !!}
                                 </div>
                                <div class="form-group">
                                    <label>USER</label>
                                    {!! Form::select('portfolio_id', $portfolioLists) !!}
                                </div>
                                {!! form::submit('Update',[' class'=>'btn btn-primary form-control'])!!}

                            </fieldset>
                        {!! Form::close()!!}

                    </div>

@stop

