@extends('admin')

@section('breadcrumb')
    <div class = "portfolio-content-header">
        <h3 class="panel-title">Add Education</h3>
    </div>
@stop

@section('adminContent')
    <div class = "Education-content">

                        {!! Form::open(['route'=>'education.store'])!!}

                            <fieldset>
                                 <div class="form-group">
                                     <label><b>SCHOOL</b></label>
                                     {!! Form::text('school',null,array('class'=>'form-control')) !!}
                                 </div>
                                 <div class="form-group">
                                     <label><b>START DATE</b></label>
                                     {!! Form::text('start_date',null,array('class'=>'form-control date-picker')) !!}
                                 </div>
                                 <div class="form-group">
                                     <label><b>END DATE</b></label>
                                     {!! Form::text('end_date',null,array('class'=>'form-control date-picker')) !!}
                                 </div>
                                <div class="form-group">
                                    <label>USER</label>
                                    {!! Form::select('portfolio_id', $portfolioLists) !!}
                                </div>

                                <center>{!! form::submit('Add',[' class'=>'btn btn-primary form-control'])!!}</center>

                            </fieldset>
                        {!! Form::close()!!}

                    </div>

@stop

