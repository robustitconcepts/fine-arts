@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Education</h3>

    <div class="sub-menu">
        <a href="{{url('education/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>

    </div>
@stop

@section('adminContent')

    <div class="table-responsive education-content">
        <table class="table table-hover table-striped">
            <thead>
            <th> SCHOOL</th>
            <th> START DATE</th>
            <th> END DATE</th>
            <th> NAME</th>
            <th> ACTIONS</th>
            </thead>

            <tbody>

                @foreach($educations as $education)
                    <tr>
                    <td>{!! $education->school!!}</td>
                    <td>{!! $education->start_date!!}</td>
                    <td>{!! $education->end_date!!}</td>
                    <td>{!! $education->portfolio->all()[0]->first_name!!}</td>
                    <td>{!! link_to_route('education.edit', '', array($education->id),array('class' => 'fa fa-pencil fa-fw')) !!}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
        <div class="pagination"> {!! $educations->render() !!} </div>
    </div>
@stop