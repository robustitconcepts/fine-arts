@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Exhibition</h3>

    <div class="sub-menu">
        <a href="{{url('exhibition/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>

    </div>
@stop

@section('adminContent')
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <th> ID</th>
            <th> TITLE</th>
            <th> DESCRIPTION</th>
            <th> YEAR</th>
            <th> NAME</th>
            <th> EDIT</th>
            </thead>

            <tbody>

                @foreach($exhibitions as $exhibition)
                    <tr>
                    <td>{!! $exhibition->id!!}</td>
                    <td>{!! $exhibition->title !!}</td>
                    <td>{!! $exhibition->description !!}</td>
                    <td>{!! $exhibition->year !!}</td>
                    <td>{!! $exhibition->portfolio->first_name!!}</td>
                    <td>{!! link_to_route('exhibition.edit', '', array($exhibition->id),array('class' => 'fa fa-pencil fa-fw')) !!}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
        <div class="pagination"> {!! $exhibitions->render() !!} </div>
    </div>
@stop

