@extends('admin')

@section('breadcrumb')
    <div class="photo-content-header">
        <h3 class="panel-title">Add Photo</h3>
    </div>
@stop

@section('adminContent')
    <div class="photo-content">

        {!! Form::open(['route'=>'photos.store','files' => true])!!}

        <div class="col-md-6">
            <div class="form-group">
                <label><b>TITLE</b></label>
                {!! Form::text('title',null,array('class'=>'form-control')) !!}
            </div>

            <div class="form-group">
                <label><b>DESCRIPTION</b></label>
                {!! Form::textarea('description',null,array('class'=>'form-control', )) !!}
            </div>

            <div class="form-group">
                <label><b>IMAGE</b></label>
                {!! Form::file('image_file',null,array('class'=>'form-control')) !!}

                {!! Form::text('image',null,array('class'=>'form-control','id'=> 'file-name','disabled' => 'disabled')) !!}
            </div>
            <div class="form-group">
                <label><b>DATE CREATED</b></label>
                {!! Form::text('date_created',null,array('class'=>'form-control date-picker')) !!}
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label><b> STATUS </b></label>
                {!! Form::select('status', ['Draft'=>'Draft','Published'=> 'Published']) !!}
            </div>
            <div class="form-group">

                <label><b>USER</b></label>
                {!! Form::select('portfolio_id', $portfolioLists) !!}

            </div>
            <div class="form-group">
                <label><b>CATEGORIES</b></label>
                 {!! Form::select('art_category_id', $arts) !!}

            </div>
            <div class="form-group">
                <label><b>ALBUM</b></label>
               {!! Form::select('album_id', $albums) !!}
            </div>
        </div>

        <center>{!! form::submit('Add',[' class'=>'btn btn-primary form-control'])!!}</center>

        {!! Form::close()!!}


    </div>

@stop

