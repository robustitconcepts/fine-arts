@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Users</h3>
    <div class="sub-menu">
        <a href="{{url('auth/register')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>
    </div>
@stop

@section('adminContent')

    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <th> ID</th>
            <th> EMAIL</th>
            <th> USERNAME</th>
            <th> EDIT</th>
            </thead>
            <tbody>

                @foreach($users as $user)
                   <tr>
                    <td>{!! $user->id !!}</td>
                    <td>{!! $user->email !!}</td>
                    <td>{!! $user->username !!}</td>
                    <td>{!! link_to_route('users.edit', '', array($user->id),array('class' => 'fa fa-pencil
                        fa-fw')) !!}
                    </td>

                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
@stop
