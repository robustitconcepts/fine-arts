@extends('admin')
@section('adminContent')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <center><b>Edit User</b></center>
                    </div>

                    <div class="panel-body">

                        {!! Form::model($edit_user, array('method' => 'PATCH','route'=>array('users.update',$edit_user->id)))
                        !!}

                        <fieldset>

                            <div class="form-group">
                                <label><b>Email</b></label>
                                {!! Form::text('email',null,array('class'=>'form-control')) !!}
                            </div>

                            <div class="form-group">
                                <label><b>User Name</b></label>
                                {!! Form::text('username',null,array('class'=>'form-control')) !!}
                            </div>

                            <div class="form-group">
                                <label><b>Password</b></label>
                                {!! Form::text('password',null,array('class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label><b>Confirmation Password</b></label>
                                {!! Form::text('password_confirmation',null,array('class'=>'form-control')) !!}
                            </div>

                            <center>{!! form::submit('Update',[' class'=>'btn btn-primary form-control'])!!}</center>

                        </fieldset>
                        {!! Form::close()!!}

                    </div>
                </div>

            </div>
        </div>
    </div><!-- /col-lg-9 END SECTION MIDDLE -->
@stop
@section('footer')
    <script>
        $('$tag_id').select2;
    </script>
@endsection

