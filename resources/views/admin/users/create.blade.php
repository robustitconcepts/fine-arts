@extends('admin')
@section('adminContent')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"><center><b>Add User</b></center></div>

                    <div class="panel-body">

                       {!! Form::open(['route'=>'userprofileadd.store'])!!}
                            <fieldset>

                                 <div class="form-group">
                                     <label><b>Email</b></label>
                                     {!! Form::text('email',null,array('class'=>'form-control')) !!}
                                 </div>

                                  <div class="form-group">
                                      <label><b>USER NAME</b></label>
                                      {!! Form::text('username',null,array('class'=>'form-control')) !!}
                                  </div>

                                  <div class="form-group">
                                         <label><b>PASSWORD</b></label>
                                         {!! Form::text('password',null,array('class'=>'form-control')) !!}
                                  </div>

                                <center>{!! form::submit('Add',[' class'=>'btn btn-primary form-control'])!!}</center>

                            </fieldset>
                        {!! Form::close()!!}

                    </div>
                </div>

        </div>
    </div>
</div><!-- /col-lg-9 END SECTION MIDDLE -->
@stop
@section('footer')
    <script>
        $('$tag_id').select2;
    </script>
@endsection

