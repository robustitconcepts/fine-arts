@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Slider</h3>
    <div class="sub-menu">
        <a href="{{url('sliders/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>
    </div>
@stop

@section('adminContent')
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <th> ID</th>
            <th> TITLE</th>
            <th> STATUS</th>
            <th> USERNAME</th>
            <th> ACTIONS</th>
            </thead>
            <tbody>
               @foreach($sliders as $slider)
                               <tr>
                                   <td>{!! $slider->id !!}</a></td>
                                   <td>{!! $slider->title !!}</a></td>
                                   <td>{!! $slider->status !!}</td>
                                   <td>{!! $slider->user->username!!}</td>

                                   <td>{!! link_to_route('sliders.edit', '', array($slider->id),array('class' => 'fa fa-pencil fa-fw')) !!}</td>

               @endforeach
                               </tr>
            </tbody>
        </table>

    </div>
@stop