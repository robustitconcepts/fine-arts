@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Albums</h3>

    <div class="sub-menu">
        <a href="{{url('albums/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>

    </div>
@stop

@section('adminContent')
    <div class="table-responsive album-content">
        <table class="table table-hover table-striped">
            <thead>
            <th> TITLE</th>
            <th> DESCRIPTION</th>
            <th> DATE CREATED</th>
            <th> USER</th>
            <th> ACTIONS</th>
            </thead>

            <tbody>

            @foreach($albums as $album)
                <tr>
                    <td><a href="{!! url('photo-album/' . $album->id) !!}">{!! $album->title !!}</a></td>
                    <td>{!! $album->description !!}</td>
                    <td>{!! $album->date_created !!}</td>
                    <td>{!! $album->portfolio->first_name !!}</td>
                    <td>{!! link_to_route('albums.edit', '', array($album->id),array('class' => 'fa fa-pencil fa-fw')) !!}</td>
            @endforeach
                </tr>
            </tbody>
        </table>
        <div class="pagination"> {!! $albums->render() !!}</div>
    </div>
@stop
