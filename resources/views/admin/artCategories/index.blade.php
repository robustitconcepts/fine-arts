@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Art Category</h3>

    <div class="sub-menu">
        <a href="{{url('artcategories/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>
    </div>
@stop

@section('adminContent')
    <div class="table-responsive art-content">
        <table class="table table-hover table-striped">
            <thead>
            <th> NAME</th>
            <th> ACTIONS</th>
            </thead>
            <tbody>
            @foreach($artcategories as $artcategory)
                <tr>
                    <td>{!! $artcategory->name!!}</td>
                    <td>{!! link_to_route('artcategories.edit', '', array($artcategory->id),array('class' => 'fa fa-pencil fa-fw')) !!}</td>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $artcategories->render() !!}</div>
    </div>
@stop