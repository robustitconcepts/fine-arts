@extends('admin')

@section('breadcrumb')
    <h3 class="panel-title">Article</h3>
    <div class="sub-menu">
        <a href="{{url('articles/create')}}"><i class="fa fa-plus-circle fa-2x pull-right"></i></a>
    </div>
@stop

@section('adminContent')
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <th> TITLE</th>
            <th> DATE CREATED</th>
            <th> TYPE</th>
            <th> STATUS</th>
            <th> ACTIONS</th>
            </thead>
            <tbody>

            @foreach($articles as $article)
                <tr>
                    <td>{!! $article->title !!}</td>
                    <td>{!! $article->date_created !!}</td>
                    <td>{!! $article->type !!}</td>
                    <td>{!! $article->status !!}</td>
                    <td>{!! link_to_route('articles.edit', '', array($article->id), array('class' => 'fa fa-pencil
                        fa-fw')) !!}
                    </td>
                    @endforeach
                </tr>
            </tbody>
        </table>
        <div class="pagination"> {!! $articles->render() !!}</div>
    </div>
@stop