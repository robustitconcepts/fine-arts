@extends('admin')

@section('breadcrumb')
    <div class="portfolio-content-header">
        <h3 class="panel-title">Edit Article</h3>
    </div>
@stop

@section('adminContent')
    <div class="Article-content">

        {!! Form::model($article,array('method' => 'PATCH','route'=>array('articles.update',$article->id))) !!}
        <fieldset>
            <div class="form-group">
                <label><b>TITLE</b></label>
                {!! Form::text('title',null,array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                <label><b>DESCRIPTIOIN</b></label>
                {!! Form::textarea('description',null,array('class'=>'form-control text-editor')) !!}
            </div>
            <div class="form-group">
                <label><b>IMAGE</b></label>
                {!! Form::file('image_file',null,array('class'=>'form-control','id'=>'file_uploader')) !!}
                {!! Form::text('image',null,array('class'=>'form-control','id'=> 'file-name')) !!}
            </div>
            <div class="form-group">
                <label><b>DATE CREATED</b></label>
                {!! Form::text('date_created',null,array('class'=>'form-control date-picker')) !!}
            </div>
            <div class="form-group">
                <label><b>TYPE</b></label>
                {!! Form::select('type', ['News'=>'News','Event'=>'Event']) !!}
            </div>
            <div class="form-group">
                <label><b>STATUS</b></label>
                {!! Form::select('status', ['Draft'=>'Draft','Published'=>'Published']) !!}
            </div>
            {!! Form::hidden('user_id') !!}

            <center>{!! form::submit('Update',[' class'=>'btn btn-primary form-control'])!!}</center>

        </fieldset>
        {!! Form::close()!!}

    </div>
    </div>

    </div>
    </div>
    </div><!-- /col-lg-9 END SECTION MIDDLE -->
@stop

