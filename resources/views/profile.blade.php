<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>creative arts</title>

    <link href="{{asset('public/css/admin-all.css')}}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    {{--<script src="cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script src="cdn.datatables.net/tabletools/2.2.3/js/dataTables.tableTools.min.js"></script>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->--}}
</head>

<body>
<div id="wrapper">

    {{--navigation section--}}
    @include('includes/admin/nav')

    <div id="page-wrapper">

        <div class="row">

            <div class="col-lg-11 main-content">

                <div class="panel panel-default">

                    @include('includes/errors')

                    @include('includes/message')

                    <div class="panel-heading">
                        @yield('breadcrumb')
                    </div>

                    <div class="panel-body main-content">
                        @yield('profileContent')
                    </div>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script src="{{ asset('public/js/admin-all.js') }}"></script>

@yield('js_code')

</body>

</html>

