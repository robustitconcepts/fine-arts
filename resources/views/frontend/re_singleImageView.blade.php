@extends('frontend')
@section('content')

    <div class="page-content single-image-view">

        <div class="col-md-9 first-row">
            <div class="col-md-8 left-col">
                <figure>
                    <img src='{{ url('public/uploads/images/' . $photo{"image"}) }}' alt="{!! $photo{'title'} !!}">
                </figure>


            </div>
            <!-- end .single-product -->
            <div class="col-md-3 right-col">

                <h4>{!! $photo{'description'} !!}</h4>
                <h6><i class="fa fa-calendar"></i> {!! $photo{'date_created'} !!}</h6>

                <h4>Description</h4>

                <p>{!! $photo{'description'} !!}</p>

                <h4>Price</h4>

                <p>NRs {!! $photo{'price'} !!}</p>

                <h4>Status</h4>

                <p>{!! $photo{'availability'} !!}</p>

                <h4>Author</h4>

                <p>{!! $photo{'user'}{'profile'}{'first_name'} !!}</p>



                <a class="read-more" href="{{url('users',  $photo{'portfolio_id'})}}"><i
                            class="fa fa-angle-double-right"></i>View Author Profile</a>
                {{-- */$id = $photo{'user'}{'profile'}{'id'};/* --}}

            </div>
            <!-- end .single-product -->

            <!-- end .single-product -->
            <div class="col-md-12 second-row" ng-app="RelatedArtWorkM">

                <h1 class="page-header">View Artist's Other Works</h1>

                @include('frontend/relatedWork')

            </div>
            <!-- end main grid layout .second-row -->


        </div>
        <!-- end main grid layout .first-row -->


        <div class="col-lg-3 right-sidebar">
            @include('frontend/joinUs')
            @include('frontend/searchMenus')
        </div>
        <!-- end right sidebar grid layout .right-sidebar -->
    </div>  <!-- end .page-content .single-image-view-->
@stop