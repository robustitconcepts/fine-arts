<div ng-class = "<% spinnerClass %>"></div>

<ul ng-repeat="imageDetail in imageDetails.data">
    <div class="art-image-listing col-md-2 col-sm-3">
        <a href="photos/<%imageDetail.id%>">
            <img class="img-responsive img-portfolio img-hover" ng-src="<% getImagePath(imageDetail.image) %>" alt="<%imageDetail.title%>">
            <div class="img-text"><%imageDetail.first_name%>  <%imageDetail.last_name%></div>
        </a>

    </div>
</ul>
@if(!isset($frontPage))
    @include('frontend/message')
    @include('frontend/pagination')
@endif

