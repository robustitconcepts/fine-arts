@extends('frontend')
@section('content')

    <div class="page-content single-artist-profile">

        <div class="col-md-9">
            <div class="col-md-12 single-user">
                <img src="{{ url('public/uploads/images/' . $artist{'image'}) }}">

                <h3>{!! $artist{'first_name'} !!}</h3>

                <h4><strong>Specialization:</strong>  {!! $artist{'art_category'}{'name'} !!}</h4>

                <h4><i class="fa fa-envelope-o"></i> {!! $artist{'user'}{'email'} !!}</h4>
            </div>

            {{-- */$id = $artist{'profile'}{'id'};/* --}}

            <div class="col-md-12 additional-desc">

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#biography" data-toggle="tab">Biography</a>
                        </li>
                        <li><a href="#exhibition" data-toggle="tab">Exhibition</a>
                        </li>
                        <li><a href="#award" data-toggle="tab">Award History</a>
                        </li>
                        <li><a href="#education" data-toggle="tab">Education</a>
                        </li>
                        <li><a href="#gallery" data-toggle="tab">Gallery</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="biography">
                            <p>{!! $artist{'profile'}{'description'} !!}</p>
                        </div>
                        <div class="tab-pane fade" id="exhibition">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Year</th>
                                </tr>
                                </thead>
                                <tbody>
                                dd($artist)
                                @foreach($artist{'exhibition'} as $exhibition_history)
                                    <tr>
                                        <td>{!! $exhibition_history{'title'} !!}</td>
                                        <td>{!! $exhibition_history{'year'} !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="award">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Year</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($artist{'award_history'} as $award_history)
                                    <tr>
                                        <td>{!! $award_history{'title'} !!}</td>
                                        <td>{!! $award_history{'year'} !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="gallery">

                            <div class="col-md-12 second-row" ng-app = "RelatedArtWorkM" >

                                @include('frontend/relatedWork')

                            </div>
                            <!-- end grid layout .second-row -->

                        </div>
                        <div class="tab-pane fade" id="education">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>School</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($artist{'education_history'} as $educationHistory)
                                    <tr>
                                        <td>{!! $educationHistory{'school'} !!}</td>
                                        <td>{!! $educationHistory{'start_date'} !!}</td>
                                        <td>{!! $educationHistory{'end_date'} !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->


            </div>
            <!-- end .additional-desc -->
        </div>
        <!-- end main grid layout -->

        <div class="col-lg-3 right-sidebar">
            @include('frontend/search')
            @include('frontend/searcha_z')
        </div>
        <!-- end right sidebar grid layout .right-sidebar -->
    </div>  <!-- end .page-content .single-artist-profile-->
@stop