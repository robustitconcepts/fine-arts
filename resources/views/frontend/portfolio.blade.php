@extends('frontend')
@section('content')

    <div class="page-content contact-us" ng-app="SearchM" ng-controller="PortfolioCtrl">
        <div class="col-md-9">
            <div class="page-header col-lg-12">
                <h2 class="col-lg-6"><i class="fa fa-paint-brush red"></i> Art Collections</h2>
            </div>

            @include('frontend/artListing')



        </div>
        <!-- end main grid layout -->

        <div class="col-lg-3 right-sidebar">
            @include('frontend/search')
            @include('frontend/categories')
        </div>
        <!-- end right sidebar grid layout .right-sidebar -->

    </div> <!-- end #page-content .portfolio -->

@stop