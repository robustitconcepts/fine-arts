@extends('frontend')
@section('content')

    <div class="page-content about-us">
        <div class="col-md-9">
            {!! $dynContent->description !!}
            {{-- <h3><strong>About</strong> CAG</h3>


             <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                     Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.
                     Cras vel lorem. Etiam pellentesque aliquet tellus.
                     Phasellus pharetra nulla ac diam. Quisque semper justo at risus.</strong></p>

             <p>Donec venenatis, turpis vel hendrerit interdum,
                 dui ligula ultricies purus, sed posuere libero dui id orci.
                 Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu,
                 vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis.
                 Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat,
                 purus sapien ultricies dolor, et mollis pede metus eget nisi.
                 Praesent sodales velit quis augue. Cras suscipit, urna at aliquam rhoncus,
                 urna quam viverra nisi, in interdum massa nibh nec erat.</p>

             <h3 class="team"> <strong>CAG</strong> Members</h3>

             <div class="row">







             </div> <!-- end .row -->--}}
        </div> <!-- end .grid .col-md-9-->
        <div class="col-lg-3 right-sidebar">
            @include('frontend/joinUs')
            @include('frontend/latestPost')
        </div>
    </div> <!-- end .about-us -->
@stop