<div class="search-form">
    <h2> Search </h2>


        <!---  Input Group TextBox -->
        <div class="input-group margin-bottom-sm">
            <span class="input-group-addon"><i class="fa fa-cogs fa-fw"></i></span>
            <input type="text" ng-enter="onKeyPress()" class="form-control" placeholder="Categories" ng-model="selCategory" typeahead="category as category.name for category in categories | filter:$viewValue | limitTo:4">

        </div>


        <!---  Input Group TextBox -->
        <div class="input-group margin-bottom-sm">
            <span class="input-group-addon"><i class="fa fa-group fa-fw"></i></span>
            <input type="text" ng-enter="onKeyPress()" class="form-control" placeholder="Artists" ng-model="selArtist" typeahead="artist as artist.first_name for artist in artists | filter:$viewValue | limitTo:4">

        </div>

        <!---  Field --->
        <div class="form-group search-button">
            <a class="btn btn-default" href ng-click="onKeyPress()"><i class="fa fa-search"></i> Search</a>
           {{-- <i class="fa fa-search"></i><input type="button" value="Search" ng-click="onKeyPress()" class ="btn btn-primary">--}}
        </div>
</div>