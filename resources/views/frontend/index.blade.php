@extends('frontend')

@section('content')
    <!-- Header Carousel -->
    <div class="row slider-block">
        <header id="myCarousel" class="carousel slide col-md-8">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @for($n = 1 ; $n <= count($images); $n++)
                    {{-- */$activeClass = ($n == 1)?'active':'';/* --}}
                    <li data-target="#myCarousel" class="{!! $activeClass !!}" data-slide-to="{!! $n !!}"></li>
                @endfor
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                {{-- */$n = 1;/* --}}
                @foreach($images as $image )
                    {{-- */$activeClass = ($n == 1)?'active':'';/* --}}

                    <div class="item {!! $activeClass !!}">
                        <div class="fill" style="background-image:url('public/uploads/slider/{!! $image->image !!}');">

                        </div>

                        <div class="carousel-caption">
                            <h6>{!! $image->title !!}</h6>
                            {!! $image->description !!}

                        </div>
                    </div>
                    {{-- */$n++;/* --}}
                @endforeach

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"></span>
            </a>
        </header>
        <ul class="latestnews col-md-3 ">
            @foreach($news as $single_news)

                <li>
                    <strong><a href="{{ url('articles/' . $single_news->id) }}">{!!$single_news->title!!}.</a></strong>
                    <p>
                        {!! substr($single_news->description,0,96)!!}
                    </p>
                </li>
            @endforeach

        </ul>
    </div>


    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Section -->
        <div class="row collection-container" ng-app="SearchM" ng-controller="PortfolioCtrl">
            <div class="page-header col-lg-12">
                <h2 class="col-lg-6"><i class="fa fa-paint-brush red"></i> Art Collections</h2>

            </div>


            @include('frontend/artListing')


        </div>
        <!-- /.row -->

        <div class="view-more">
            <a href="portfolio"><i class="fa fa-plus-square-o"></i>View More</a>
        </div>

        <!-- Marketing Icons Section -->
        <div class="row">

            <div class="col-md-4">
                <a class="btn btn-lg btn-default btn-block" href="portfolio">Artists A-Z</a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-lg btn-default btn-block" href="portfolio">Artist By Category</a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-lg btn-default btn-block" href="auth/register">Artists Join Us</a>
            </div>
        </div>
        <!-- /.row -->


        <hr>

    </div>
    <!-- /.container -->
@stop