@extends('frontend')
@section('content')

    <div ng-class = "spinnerClass"></div>
    <div class="page-content artist-search" ng-app="SearchM" ng-controller="ArtistViewCtrl">
        <div class="col-md-9">
                <h2>Artists</h2>

                <div class="single-product" ng-repeat="artist in artists">

                    <h2><% artist.first_name %></h2>

                    <h6><a href="#"><% artist.art_category %></a></h6>

                    <p><% artist.description %></p>

                    <a class="btn btn-default" ng-href="users/<% artist.id %>"><i class="fa fa-angle-double-right"></i> View Artist Profile</a>

                </div>
                <!-- end .single-product -->
                @include('frontend/message')
                @include('frontend/pagination')


        </div>
        <!-- end main grid layout -->

        <div class="col-lg-3 right-sidebar">
            @include('frontend/search')
            @include('frontend/searcha_z')
        </div>
        <!-- end right sidebar grid layout .right-sidebar -->

    </div> <!-- end #page-content .portfolio -->








@stop