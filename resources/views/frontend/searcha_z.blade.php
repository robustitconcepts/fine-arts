<div class="alpha-search">
    <h2>A-Z SEARCH</h2>
    <ul class="main-l-nav-alphabet">
        <li><a target="_self" href ng-click="searchArtistByAlphabet('a')">A</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('b')">B</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('c')">C</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('d')">D</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('e')">E</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('f')">F</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('g')">G</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('h')">H</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('i')">I</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('j')">J</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('k')">K</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('l')">L</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('m')">M</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('n')">N</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('o')">O</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('p')">P</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('q')">Q</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('r')">R</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('s')">S</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('t')">T</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('u')">U</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('v')">V</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('w')">W</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('x')">X</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('y')">Y</a></li>
        <li><a target="_self" href ng-click="searchArtistByAlphabet('z')">Z</a></li>
    </ul>
</div>