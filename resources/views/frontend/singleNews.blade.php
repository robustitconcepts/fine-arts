@extends('frontend')
@section('content')


    <div class="page-content artist-search">

        <div class="col-md-9">
            <h2>News</h2>

            <div class="single-product">
                <h4>{!! $news->title!!}</h4>
                <h6>{!!$news->date_created!!}</h6>

                <p>{!!$news->description!!} </p>
            </div>
            <!-- end .single-product -->


        </div>
        <!-- end main grid layout -->

        <div class="col-lg-3 right-sidebar">
            @include('frontend/joinUs')
            @include('frontend/searchMenus')
        </div>
        <!-- end right sidebar grid layout .right-sidebar -->

    </div> <!-- end #page-content .portfolio -->


@stop