/**
 * Created by shashi on 4/19/15.
 */

$(function () {
    $('#side-menu').metisMenu();
    $('.date-picker').datepicker();
    $('.thumbnail > a').colorbox({rel:'gal'});
    $('#tag_id').select2({
        placeholder:'choose a tag',
        tags:true
    });
    $('input[type="file"]').on('change',function(){
       $("#file-name").val(this.value);
    });
    $('.text-editor').markItUp(mySettings);
    $('.delete-photo').confirm({
        text: "Are you sure you want to delete this photo?",
        width: 100,
        title: "Delete Confirmation",
        confirm: function(button) {

        },
        confirmButton: "Yes",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg"
    });

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function () {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function () {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});
