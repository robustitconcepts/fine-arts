SET foreign_key_checks = 0;


INSERT INTO `albums` (`id`, `title`, `description`, `date_created`, `portfolio_id`, `created_at`, `updated_at`) VALUES
(1, 'Album1', 'Album1', '2015-05-29', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `date_created`, `type`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Oil Painting', 'about oil painting', '1993-01-03', 'news', 'onHold', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Nepal''s national art collection at risk in building damaged by earthquake', 'Academy of Fine Arts building in Kathmandu reported to be in danger of collapse and contains 700 paintings, some of which date back to the time of Buddha\r\nAcademy of Fine Arts building in Kathmandu reported to be in danger of collapse and contains 700 pai', '2015-05-13', 'news', 'published', 1,'2015-05-12 12:30:00', '2015-05-12 12:30:00'),
(3, 'Bethesda Tattoo Artist Survived Earthquake in Nepal', 'Matthew Wojciechiwski just got back to work in Bethesda. But he''s not the same guy who left for Nepal.\r\n\r\nHe survived the massive earthquake there that killed about 7,000 people and destroyed entire villages April 25. He returned to the U.S. this week.\r\n\r', '2015-05-13', 'news', 'publishe', 1,'0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Nepal earthquake: Artistic boy aged 5 sells his drawings worldwide to help stranded children ', 'A five-year-old artist is selling his perfect paintings to help children caught up in the Nepal earthquake.\r\n\r\nAnd Charlie McMillan'' mum Laura says he has around 50 commissions so far!\r\n\r\nThe pupil set himself a target of £50 but has already raised over £', '2015-05-13', 'news', 'published', 1,'0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `art_categories`
--

INSERT INTO `art_categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Oil Painting', 'Oil painting is the process of painting with pigments that are bound with a medium of drying oil—especially in early modern Europe, linseed oil. Often an oil such as linseed was boiled with a resin such as pine resin or even frankincense; these were calle', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Pastel', 'Pastel is a painting medium in the form of a stick, consisting of pure powdered pigment and a binder.[14] The pigments used in pastels are the same as those used to produce all colored art media, including oil paints; the binder is of a neutral hue and lo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Acrylic Painting', 'Acrylic paint is fast drying paint containing pigment suspension in acrylic polymer emulsion. Acrylic paints can be diluted with water, but become water-resistant when dry. Depending on how much the paint is diluted (with water) or modified with acrylic g', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Watercolor Painting', 'Watercolor is a painting method in which the paints are made of pigments suspended in a water soluble vehicle. The traditional and most common support for watercolor paintings is paper; other supports include papyrus, bark papers, plastics, vellum or leat', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Ink Painting', 'Ink paintings are done with a liquid that contains pigments and/or dyes and is used to color a surface to produce an image, text, or design. Ink is used for drawing with a pen, brush, or quill. Ink can be a complex medium, composed of solvents, pigments, ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Hot Wax', 'Encaustic painting, also known as hot wax painting, involves using heated beeswax to which colored pigments are added. The liquid/paste is then applied to a surface—usually prepared wood, though canvas and other materials are often used. The simplest enca', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Fresco', 'Fresco is any of several related mural painting types, done on plaster on walls or ceilings. The word fresco comes from the Italian word affresco [afˈfresːko], which derives from the Latin word for fresh. Frescoes were often made during the Renaissance an', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Gouache', 'Gouache is a water based paint consisting of pigment and other materials designed to be used in an opaque painting method. Gouache differs from watercolor in that the particles are larger, the ratio of pigment to water is much higher, and an additional', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `award_histories`
--

INSERT INTO `award_histories` (`id`, `title`, `year`, `portfolio_id`, `created_at`, `updated_at`) VALUES
(1, 'Vidhya Bhusan ', '1993', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `dynamic_contents`
--

INSERT INTO `dynamic_contents` (`id`, `title`, `description`, `status`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'aboutUs', '<h3><strong>About</strong> CAG</h3>\n\n <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                        Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.\n                        Cras vel lorem. Etiam pellentesque aliquet tellus.\n                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.</strong></p>\n\n                <p>Donec venenatis, turpis vel hendrerit interdum,\n                    dui ligula ultricies purus, sed posuere libero dui id orci.\n                    Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu,\n                    vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis.\n                    Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat,\n                    purus sapien ultricies dolor, et mollis pede metus eget nisi.\n                    Praesent sodales velit quis augue. Cras suscipit, urna at aliquam rhoncus,\n                    urna quam viverra nisi, in interdum massa nibh nec erat.</p>\n\n                <h3 class="team"> <strong>CAG</strong> Members</h3>\n\n                <div class="row">\n\n\n\n\n\n\n\n                </div> <!-- end .row -->', 'published', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'contactUs', '      <h3><strong>ADDRESS</strong> DETAILS</h3>\n\n                            <div class="address-details clearfix">\n                                <i class="fa fa-map-marker"></i>\n\n                                <p>\n                                    <span>1234 Hyde Street</span>\n                                    <span>San Francisco</span>\n                                    <span>CA 94043</span>\n                                </p>\n                            </div>\n\n                            <div class="address-details clearfix">\n                                <i class="fa fa-phone"></i>\n\n                                <p>\n                                    <span><strong>Phone:</strong> +1 123-456-7890</span>\n                                    <span><strong>Fax:</strong> +1 123-456-7891</span>\n                                </p>\n                            </div>\n\n                            <div class="address-details clearfix">\n                                <i class="fa fa-envelope-o"></i>\n\n                                <p>\n                                    <span><strong>E-mail:</strong> example@example.com</span>\n                                    <span><span><strong>Website:</strong> www.example.com</span></span>\n                                </p>\n                            </div>\n\n                            <h3>Opening Hours</h3>\n\n                            <div class="address-details clearfix">\n                                <i class="fa fa-clock-o"></i>\n\n                                <p>\n                                    <span><strong>Mo-Fri:</strong> 9AM - 5PM</span>\n                                    <span><span><strong>Saturday:</strong> 10AM - 2PM</span></span>\n                                    <span><strong>Sunday:</strong> Closed</span>\n                                </p>\n                            </div>\n', 'published', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------


--
-- Dumping data for table `education_histories`
--

INSERT INTO `education_histories` (`id`, `school`, `start_date`, `end_date`, `portfolio_id`, `created_at`, `updated_at`) VALUES
(1, 'Hindu Vidyapeeth Nepal', '0000-00-00', '0000-00-00', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `exhibitions`
--

INSERT INTO `exhibitions` (`id`, `title`, `description`, `year`, `portfolio_id`, `created_at`, `updated_at`) VALUES
(1, 'Pulchowk Engineering Science & Art Exhibition', 'Exhibition', '2010', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `menu_name`, `menu_type`, `menu_url`, `parent_id`, `sorting_order`, `created_at`, `updated_at`, `icon`) VALUES
(1, 'Users', 'Admin', 'portfolios', 0, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-user fa-fw'),
(2, 'Articles', 'Admin', 'articles', 0, '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-book fa-fw'),
(3, 'Albums', 'User', 'albums', 0, '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-pencil fa-fw'),
(4, 'Education', 'User', 'education', 0, '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-book fa-fw'),
(5, 'Exhibition', 'User', 'exhibition', 0, '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-pencil fa-fw'),
(6, 'Award Histories', 'User', 'awardhistories', 0, '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-book fa-fw'),
(7, 'Art Categories', 'Admin', 'artcategories', 0, '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-table fa-fw'),
(8, 'Slider', 'Admin', 'sliders', 0, '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-picture-o fa-fw');
(9, 'Dynamic Menus', 'Dynamic Menus', 'dynamiccontents', 0, '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fa fa-book fa-fw');

-- --------------------------------------------------------


--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`role_id`, `menu_id`) VALUES
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(8, 8),
(8, 9),
(7, 3),
(7, 4),
(7, 5),
(7, 6);


--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(7, 'create-post', 'Create Posts', 'create new blog posts', '2015-05-12 20:47:59', '2015-05-12 20:47:59'),
(8, 'edit-user', 'Edit Users', 'edit existing users', '2015-05-12 20:47:59', '2015-05-12 20:47:59');

-- --------------------------------------------------------

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 1),
(2, 1),
(3, 4),
(3, 3),
(4, 3),
(5, 6),
(5, 5),
(6, 5),
(7, 8),
(7, 7),
(8, 7);

-- --------------------------------------------------------

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `title`, `description`, `image`, `date_created`, `status`, `portfolio_id`, `art_category_id`, `album_id`, `created_at`, `updated_at`, `availability`, `price`) VALUES
(17, 'Photo1', 'photo1', '17.png', '2012-01-03', 'Draft', 2, 1, 1, '2015-05-07 19:25:55', '2015-05-07 19:25:55', 'Available', 2388),
(18, 'photo2', 'photo2', '18.png', '2012-01-03', 'Draft', 2, 1, 1, '2015-05-07 19:26:15', '2015-05-07 19:26:15', 'Available', 2388),
(19, 'photo3', 'photo3', '19.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 20:28:02', '2015-05-07 20:28:02', 'Available', 1088),
(20, 'photo4', 'photo4', '20.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 20:28:24', '2015-05-07 20:28:24', 'Available', 2388),
(21, 'photo5', 'photo5', '21.png', '2012-01-03', 'Published', 2, 1, 1, '2015-05-07 20:49:02', '2015-05-07 20:49:02', 'Available', 2388),
(22, 'photo5', 'photo5', '22.png', '2012-01-03', 'Published', 2, 1, 1, '2015-05-07 20:50:41', '2015-05-07 20:50:41', 'Available', 3288),
(23, 'photo5', 'photo5', '23.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 20:51:55', '2015-05-07 20:51:55', 'Available', 2388),
(24, 'photo10', 'photo10', '24.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 20:53:31', '2015-05-07 20:53:31', 'Available', 5388),
(25, 'photo11', 'photo11', '25.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 20:56:50', '2015-05-07 20:56:50', 'Available', 2388),
(26, 'photo12', 'photo12', '26.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 20:59:08', '2015-05-07 20:59:08', 'Available', 2388),
(27, 'photo13', 'photo13', '27.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 21:01:38', '2015-05-07 21:01:38', 'Available', 56388),
(28, 'photo13', 'photo13', '28.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 21:35:47', '2015-05-07 21:35:47', 'Available', 2388),
(29, 'photo119', 'photo119', '29.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-08 00:08:12', '2015-05-08 00:08:12', 'Available', 2388),
(30, 'photo4', 'photo4', '20.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 14:43:24', '2015-05-07 14:43:24', 'Available', 56388),
(31, 'photo5', 'photo5', '21.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:04:02', '2015-05-07 15:04:02', 'Available', 2388),
(32, 'photo5', 'photo5', '22.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:05:41', '2015-05-07 15:05:41', 'Available', 2388),
(33, 'photo5', 'photo5', '23.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:06:55', '2015-05-07 15:06:55', 'Available', 6388),
(34, 'photo10', 'photo10', '24.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:08:31', '2015-05-07 15:08:31', 'Available', 2388),
(35, 'photo11', 'photo11', '25.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:11:50', '2015-05-07 15:11:50', 'Available', 8388),
(36, 'photo12', 'photo12', '26.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:14:08', '2015-05-07 15:14:08', 'Available', 2388),
(37, 'photo1', 'photo1', '17.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 13:40:55', '2015-05-07 13:40:55', 'Available', 2388),
(38, 'photo2', 'photo2', '18.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 13:41:15', '2015-05-07 13:41:15', 'Available', 9388),
(39, 'photo3', 'photo3', '19.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 14:43:02', '2015-05-07 14:43:02', 'Available', 2388),
(47, 'photo13', 'photo13', '27.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:16:38', '2015-05-07 15:16:38', 'Available', 2388),
(48, 'photo13', 'photo13', '28.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 15:50:47', '2015-05-07 15:50:47', 'Available', 2000),
(49, 'photo119', 'photo119', '29.png', '2012-01-03', 'Published', 1, 1, 1, '2015-05-07 18:23:12', '2015-05-07 18:23:12', 'Available', 2388),
(54, 'New Image', 'This is a new Image', '54.jpg', '2015-06-01', 'Published', 1, 1, 1, '2015-06-01 22:50:27', '2015-06-01 22:50:27', '', 0);

-- --------------------------------------------------------

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `first_name`, `last_name`, `art_category_id`, `user_id`, `contact_no`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'CAG Admin', 'CAG Admin', 1, 1, 2147483647, 'CAG Admin was born in 1985 in Nitra, Slovakia. She practices her art in Vienna and since 2012 she has a second home in Zaragoza, Spain.\n\nShe discovered a passion for painting when she was a small child, her favorite toys being crayons, brushes, canvases, and ', '17.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Amrit ', 'Amrit', 1, 2, 2147483647, 'Amrit was born in 1985 in Nitra, Slovakia. She practices her art in Vienna and since 2012 she has a second home in Zaragoza, Spain.\n\nShe discovered a passion for painting when she was a small child, her favorite toys being crayons, brushes, canvases, and ', '18.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(7, 'Artist', 'Artist', 'Artist profile to manage account and art gallery. ', '2015-05-12 20:47:59', '2015-05-12 20:47:59'),
(8, 'Admin', 'User Administrator', 'User is allowed to manage and edit other users', '2015-05-12 20:47:59', '2015-05-12 20:47:59');

-- --------------------------------------------------------

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(8, 1),
(7, 2);

-- --------------------------------------------------------

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `status`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Nullamlacus dui ipsum cons eque loborttis', '<a class="btn btn-default" href="#">Join Us Now</a>', 'published', 'public/uploads/slider/banner-1.png',  1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Nullamlacus dui ipsum cons eque loborttis', ' <a class="btn btn-default" href="#">Find a Artist</a>', 'published', 'public/uploads/slider/banner-2.png', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'uncatogorised', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'news', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'cagadmin@gmail.com', 'CAG Admin', '$2y$10$PHdxOq7gDf3m5pkLOn5xqOPatDitwTpU9yJCzucIoR1s10/ozTO0m', 'QSBlBSdKUk870OBXR3U1bvG9Smfm5DZITbmDMcPG8jgJWaj7BINVrE31Di0B', '2015-05-12 20:47:58', '2015-06-02 04:12:30', 'Active'),
(2, 'amrit@gmail.com', 'Amrit', '$2y$10$FoKBMu9EiwerV4mmpfQcQ.QzmZCu5jPW7Zyroz/cOjTQde5wXYKOW', 'ps9ZiSPzY6FSY7dwlS6tdxSJ3hEsP1wOrU6MRSbQQL4LNah5w6xxtWgkdnz7', '2015-05-12 20:47:58', '2015-06-01 02:17:32', 'Active');

SET foreign_key_checks = 1;
