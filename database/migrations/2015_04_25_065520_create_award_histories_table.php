<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('award_histories', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('year');
            $table->integer('portfolio_id')->unsigned()->index;
            $table->foreign('portfolio_id')->references('id')->on('portfolios');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('award_histories');
	}

}
