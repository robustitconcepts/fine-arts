<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('education_histories', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('school');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('portfolio_id')->unsigned()->index;
            $table->foreign('portfolio_id')->references('id')->on('portfolios');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('education_histories');
	}

}
