<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('photos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->text('image');
            $table->date('date_created');
            $table->string('status');
            $table->string('availability', 25);
            $table->float('price');
            $table->integer('portfolio_id')->unsigned()->index();
            $table->foreign('portfolio_id')->references('id')->on('portfolios');
            $table->integer('art_category_id')->unsigned()->index();
            $table->foreign('art_category_id')->references('id')->on('art_categories');
            $table->integer('album_id')->unsigned()->index();
            $table->foreign('album_id')->references('id')->on('albums');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('photos');
	}

}
