<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        view()->composer('admin.*','App\Http\ViewComposers\AdminComposer');
        view()->composer('profile.*','App\Http\ViewComposers\AdminComposer');

        view()->composer('*', 'App\Http\ViewComposers\GlobalComposer');
        view()->composer('profile.*', 'App\Http\ViewComposers\PortfolioComposer');
        view()->composer('admin.*', 'App\Http\ViewComposers\PortfolioComposer');


    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}