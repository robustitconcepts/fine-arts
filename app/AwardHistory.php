<?php namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
/**
 * Class AwardHistory
 * @package App
 */
class AwardHistory extends Model {

    protected $guarded = [];



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function portfolio(){
        return $this->belongsTo('App\Portfolio');
    }

    /**
     * @param bool $excludeDeleted
     * @return $this
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery($excludeDeleted);
        if(Auth::check() && (!Auth::user()->hasRole('admin')))
            return $query->where('portfolio_id', Auth::user()->profile->id);
        else
            return $query;
    }

}
