<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{

    protected $guarded = [];

    public function art_category()
    {
        return $this->belongsTo('App\ArtCategory');
    }

    public function education_history()
    {
        return $this->hasMany('App\EducationHistory');
    }

    public function award_history()
    {
        return $this->hasMany('App\AwardHistory');
    }

    public function exhibition()
    {
        return $this->hasMany('App\Exhibition');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function photo()
    {
        return $this->hasMany('App\Photo');
    }
    public function album()
    {
        return $this->hasMany('App\Album');
    }

}
