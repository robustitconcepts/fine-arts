<?php namespace App\Http\Controllers;


use App\Http\Requests;

use App\Repository\ArtCategory as ArtCategory;
use App\Http\Requests\CreatePhotoRequest;
use App\Repository\PortfolioRepository as Portfolio;
use App\Repository\AlbumRepository as Album;
use App\Repository\PhotoRepository as Photo;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Repository\UserRepository as User;

use Session;

/**
 * Class PhotoController
 * @package App\Http\Controllers
 */
class PhotoController extends Controller
{
    /**
     * @var int
     */
    private $count;


    /**
     *
     */
    public function __construct()
    {

        $this->count = 10;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Photo $photo)
    {


        $photos = $photo->paginate($this->count);

        return view('admin.photos.index', compact('photos'));
    }


    /**
     * @param User $user
     * @param Album $album
     * @param ArtCategory $artCategory
     * @return \Illuminate\View\View
     */
    public function create(User $user, Album $album, ArtCategory $artCategory)
    {

        $all_users = $user->all();
        $albums = $album->lists('title','id');
        //$arts = $artCategory->lists('id','');
        $arts=$artCategory->all();
        return view('admin.photos.create', compact('all_users', 'albums','arts'));
    }

    /**
     * @param CreatePhotoRequest $request
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreatePhotoRequest $request, Photo $photo)
    {
         $uploaded_file = $request->file('image_file');
        $ext = $uploaded_file->getClientOriginalExtension();

        $parameter = $request->all();

        unset($parameter['image_file']);

        $photo = $photo->create($parameter);
        $imageName = $photo->id . "." . $ext;


        $uploaded_file->move(
            base_path() . '/public/uploads/images', $imageName
        );

        Image::make(base_path() . '/public/uploads/images/' . $imageName, array(
            'width' => 190,
            'height' => 121,
        ))->save(base_path() . '/public/uploads/thumbnails/' . $imageName);


        $photo->update(array('image' => $imageName));
        Session::flash('message', 'The photo was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('photos');
    }


    /**
     * @param $id
     * @param Photo $photo
     * @return \Illuminate\View\View
     */
    public function show($id, Photo $photo)
    {
        $photo = $photo->find($id);

        return view('frontend.singleImageView', compact('photo'));
    }


    /**
     * @param $id
     * @param Photo $photo
     * @param User $user
     * @param Album $album
     * @param ArtCategory $artCategory
     * @return $this
     */
    public function edit($id, Photo $photo, User $user, Album $album, ArtCategory $artCategory)
    {
      $albums = $album->lists('title','id');

        return view('admin.photos.edit')->with('photo', $photo->find($id))->with('users', $user->all())->with('albums',
            $albums);
    }


    /**
     * @param $id
     * @param CreatePhotoRequest $request
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, CreatePhotoRequest $request, Photo $photo)
    {
        $photo_to_update = $photo->find($id);
        $uploaded_image = $request->file('image_file');
        $parameter = $request->all();

        if (isset($uploaded_image)) {

            $ext = $uploaded_image->getClientOriginalExtension();
            $newImageName = $photo_to_update->id . "." . $ext;

            $uploaded_image->move(
                base_path() . '/public/uploads/images/', $newImageName
            );
            Image::make(base_path() . '/public/uploads/images/' . $newImageName, array(
                'width' => 774,
                'height' => 329,
            ))->save(base_path() . '/public/uploads/thumbnails/' . $newImageName);
            unset($parameter['image_file']);
            $parameter['image'] = $newImageName;
            $photo_to_update->update($parameter);

        } else {
            $parameter['image'] = $photo_to_update->image;
            $photo_to_update->update($parameter);
        }

        Session::flash('message', 'The content was successfully updated!.');
        Session::flash('flash_type', 'alert-success');


        return redirect('photos');
    }

    /**
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        //
        return "deleted";
    }

    /**
     * @param Photo $photo
     * @param ArtCategory $category
     * @param Portfolio $portfolio
     * @param Request $request
     * @return mixed
     */
    public function getPhotosByCategoryNArtist(Photo $photo, ArtCategory $category, Portfolio $portfolio, Request $request)
    {

        $searchCategory = ($request->input('category')) ? $request->input('category') : "%";
        $searchArtist = ($request->input('artist_name')) ? $request->input('artist_name') : "%";

        return $photo->findByCategoryAndArtist($category, $portfolio, array($searchCategory, $searchArtist));
    }


    /**
     * @param Photo $photo
     * @param Request $request
     * @return static
     */
    public function getArtistPhotos(Photo $photo, Request $request)
    {
        $artistID = $request->input('artist_id');
        $recCount = $request->input('rec_count');

        return $photo->getArtistPhotos($artistID, $recCount);
    }

    /**
     * @param Photo $photo
     * @param $albumID
     * @return \Illuminate\View\View
     */
    public function getPhotosByAlbum(Photo $photo, $albumID)
    {
        $photos = $photo->getPhotosByAlbum($photo, $albumID, 18);


        return view('admin.photos.index', compact('photos'));
    }

}
