<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePortfolioRequest;
use Folklore\Image\Facades\Image;
use App\Http\Requests\CreateUploadPortfolioRequest;
use App\Repository\PortfolioRepository as Portfolio;
use App\Repository\ArtCategory as ArtCategory;
use App\Repository\TagRepository as Tag;
use Illuminate\Http\Request;
use App\Repository\UserRepository as User;
use Session;

/**
 * Class PortfolioController
 * @package App\Http\Controllers
 */
class PortfolioController extends Controller
{
    /**
     * @var int
     */
    private $count;

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     *
     */
    public function __construct()
    {

        $this->count = 5;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Portfolio $portfolio)
    {
        $portfolios = $portfolio->paginate($this->count);

        return view('admin.portfolios.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(ArtCategory $artCategory, Tag $tag)
    {

        return view('admin.portfolios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePortfolioRequest $request
     * @return Response
     */
    public function store(CreatePortfolioRequest $request, Portfolio $portfolio, User $user, Tag $tag)
    {
        $newPortfolio = $portfolio->create(
            array(
                "first_name" => $request->input('first_name'),
                "last_name" => $request->input('last_name'),
                "art_category_id" => $request->input('art_category_id'),
                "contact_no" => $request->input('contact_no'),
                "description" => $request->input('description'),
                "user_id" => $request->input('user_id')
            ));

        $tags = $request->input('tags');

        $newPortfolio->tags()->attach($tags);


        Session::flash('message', 'The profile was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        $logged_in_user = $user->find($request->input('user_id'));
        if ($logged_in_user->hasRole(array('admin'))) {
            return redirect('portfolios');
        } else {
            return redirect('dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, Portfolio $portfolio)
    {

        $artist = $portfolio->findBy('id', $id);

        return view('admin.portfolios.profile', compact('artist'));


        //return view('frontend.singleArtistProfile',compact('artist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id, Portfolio $portfolio, ArtCategory $art, Tag $tag)
    {
        return view('admin.portfolios.edit')->with('portfolio', $portfolio->find($id))->with('arts',
            $art->all())->with('tags', $tag->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, CreatePortfolioRequest $request, Portfolio $portfolio)
    {
        $portfolio->find($id)->update(array(
            "first_name" => $request->input('first_name'),
            "last_name" => $request->input('last_name'),
            "art_category_id" => $request->input('art_category_id'),
            "contact_no" => $request->input('contact_no'),
            "description" => $request->input('description'),
            "user_id" => $request->input('user_id')
        ));

        Session::flash('message', 'The Portfolio  was successfully Updated!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('portfolios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param ArtCategory $category
     * @param Portfolio $portfolio
     * @param Request $request
     * @return mixed
     */
    public function getArtistsByCategoryNArtist(ArtCategory $category, Portfolio $portfolio, Request $request)
    {
        $searchCategory = ($request->input('category')) ? $request->input('category') : "%";
        $searchArtist = ($request->input('artist_name')) ? $request->input('artist_name') : "%";

        return $portfolio->findByCategoryNArtist($category, array($searchCategory, $searchArtist));

    }

    /**
     * @param Request $request
     * @param Portfolio $portfolio
     * @return mixed
     */
    public function searchArtistByAlphabet(Request $request, Portfolio $portfolio)
    {
        return $portfolio->searchArtistByAlphabet($request->input('alphabet'));
    }

    /**
     * @param CreateUploadPortfolioRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProfile(CreateUploadPortfolioRequest $request, Portfolio $portfolio, User $user)
    {

        $ext = $request->file('image')->getClientOriginalExtension();

        $id = $request->input('id');

        $imageName = $id . "." . $ext;

        $request->file('image')->move(
            base_path() . '/public/uploads/images', $imageName
        );

        Image::make(base_path() . '/public/uploads/images/' . $imageName, array(
            'width' => 190,
            'height' => 121,
        ))->save(base_path() . '/public/uploads/images/' . $imageName);


        $portfolio->find($id)->update(array('image' => $imageName));


        Session::flash('message', 'The profile was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('dashboard');
    }

    /**
     * @param Portfolio $portfolio
     * @return mixed
     */
    public function getAllArtist(Portfolio $portfolio)
    {
        return $portfolio->all(array('id', 'first_name'));
    }


}