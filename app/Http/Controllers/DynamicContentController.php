<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateDynamicContentRequest;
use App\Repository\DynamicContentRepository as DynamicContent;
use App\Repository\ArticleRepository as Article;
use Folklore\Image\Facades\Image;


use Session;


/**
 * Class DynamicContentController
 * @package App\Http\Controllers
 */
class DynamicContentController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | DynamicContentController
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(DynamicContent $dynamic)
    {
        return view('admin.dynamics.index')->with('dynamics', $dynamic->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.dynamics.create');
    }


    /**
     * @param CreateDynamicContentRequest $request
     * @param DynamicContent $dynamic
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateDynamicContentRequest $request, DynamicContent $dynamic)
    {

        $dynamic = $dynamic->create($request->all());

        Session::flash('message', 'The content was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('dynamiccontents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, DynamicContent $dynamicContent, Article $article)
    {
        $articles = $article->getArticlesByPublishedDate(5);
        $dynContent = $dynamicContent->find($id);

        return view('frontend.' . $dynContent['title'], compact('articles', 'dynContent'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id, DynamicContent $dynamic)
    {
        return view('admin.dynamics.edit')->with('dynamics', $dynamic->find($id));
    }


    /**
     * @param $id
     * @param DynamicContent $dynamic
     * @param CreateDynamicContentRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, DynamicContent $dynamic, CreateDynamicContentRequest $request)
    {
        $dynamic->find($id)->update($request->all());

        return redirect('dynamiccontents');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}