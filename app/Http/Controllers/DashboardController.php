<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\UserRepository as User;
use Illuminate\Contracts\Auth\Guard as Auth;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | DashboardController
    |--------------------------------------------------------------------------
    |
    | The controller handles all the dashboard activities
    |
    */

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {


    }

    /**
     * @return \Illuminate\View\View
     */
    public function index(User $user, Auth $auth)
    {

        $loggedin_user = $auth->user();
        if ((!$user->hasProfile($loggedin_user->id)) && (!$loggedin_user->hasRole(['admin']))) {

            return view('profile.create');
        } else {

            return view('admin.dashboard');
        }
    }


}
