<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repository\UserRepository as User;
use App\Repository\PortfolioRepository as Portfolio;
use App\Http\Requests\CreateUserRequest;

use Session;

use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return Response
     */
    public function index(User $user)
    {

        return view('admin.users.index')->with('users', $user->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CreateUserRequest $request
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, Portfolio $portfolio)
    {

        $artist = $portfolio->findBy('id', $id);

        return view('frontend.singleArtistProfile', compact('artist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id, User $user)
    {
        return view('admin.users.edit')->with('edit_user', $user->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, CreateUserRequest $request, User $user)
    {
        $user->find($id)->update($request->all());

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @param $status
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id, $status, User $user)
    {
        $user->update(array('status' => $status), $id);


        Session::flash('message', "The status was successfully updated to $status.");
        Session::flash('flash_type', 'alert-success');

        return redirect('portfolios');

    }

}
