<?php namespace App\Http\Controllers;

use App\Portfolio;
use App\Repository\ExhibitionRepository as Exhibition;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateExhibitionRequest;
use Request;
use Session;

class ExhibitionController extends Controller
{

    /**
     * @var int
     */
    private $count;

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->count = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Exhibition $exhibition
     * @return Response
     */
    public function index(Exhibition $exhibition)
    {

        $exhibitions = $exhibition->paginate($this->count);

        return view('admin.exhibitions.index', compact('exhibitions'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Portfolio $portfolio)
    {
        return view('admin.exhibitions.create')->with('portfolios', $portfolio->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateExhibitionRequest $request
     *
     * @return Response
     */

    public function store(CreateExhibitionRequest $request, Exhibition $exhibition)
    {
        $exhibition->create($request->all());
        Session::flash('message', 'The Exhibition was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('exhibition');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id, Exhibition $exhibition, Portfolio $portfolio)
    {

        return view('admin.exhibitions.edit')->with('exhibition', $exhibition->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, CreateExhibitionRequest $request, Exhibition $exhibition)
    {
        $exhibition->find($id)->update($request->all());
        Session::flash('message', 'The Exhibition was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('exhibition');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllExhibition(Exhibition $exhibition)
    {
        return \response::json($exhibition->all());
    }
}
