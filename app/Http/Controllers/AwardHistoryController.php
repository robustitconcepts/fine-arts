<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Portfolio;
use App\Repository\AwardHistoryRepository as AwardHistory;
use App\Http\Requests\CreateAwardHistoryRequest;
use Request;
use Session;

/**
 * Class AwardHistoryController
 * @package App\Http\Controllers
 */
class AwardHistoryController extends Controller
{
    /**
     * @var int
     */
    private $count;

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->count = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(AwardHistory $awardHistory)
    {
        $awardhistories = $awardHistory->paginate($this->count);
        return view('admin.awardHistories.index',compact('awardhistories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Portfolio $portfolio)
    {
        return view('admin.awardHistories.create')->with('portfolios',$portfolio->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateAwardHistoryRequest $request
     * @return Response
     */
    public function store(CreateAwardHistoryRequest $request, AwardHistory $awardHistory)
    {

        $awardHistory->create($request->all());
        Session::flash('message', 'The Award History was successfully added!.');
        Session::flash('flash_type', 'alert-success');
        return redirect('awardhistories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, AwardHistory $awardHistory)
    {
        return view('admin.awardHistories.index')->with('awardhistories', $awardHistory->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id, AwardHistory $awardHistory,Portfolio $portfolio)
    {
        return view('admin.awardHistories.edit')->with('awardhistories', $awardHistory->find($id));
    }

    /**
     * @param  int $id
     * @return Response
     */
    public function update($id, CreateAwardHistoryRequest $request, AwardHistory $awardHistory)
    {

        $awardHistory->find($id)->update($request->all());
        Session::flash('message', 'The Award Histories was successfully updated!.');
        Session::flash('flash_type', 'alert-success');
        return redirect('awardhistories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param AwardHistory $awardHistory
     * @return mixed
     */
    public function getAllAward(AwardHistory $awardHistory)
    {
        return $awardHistory->all();
    }

}
