<?php namespace App\Http\Controllers;

use App\Portfolio;
use App\Repository\EducationHistoryRepository as EducationHistory;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEducationHistoryRequest;
use Illuminate\Contracts\Auth\Guard as Auth;


use Session;

class EducationHistoryController extends Controller
{
    /**
     * @var int
     */
    private $count;

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->count = 10;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(EducationHistory $educationHistory)
    {

        $educations = $educationHistory->paginate($this->count);

        return view('admin.educationHistories.index', compact('educations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Portfolio $portfolio
     * @return Response
     */
    public function create(Portfolio $portfolio)
    {
        return view('admin.educationHistories.create')->with('portfolios', $portfolio->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateEducationHistoryRequest $request
     * @param EducationHistory $education
     * @return Response
     */
    public function store(CreateEducationHistoryRequest $request, EducationHistory $education)
    {
        $education->create($request->all());
        Session::flash('message', 'The Education was successfully added!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('education');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param EducationHistory $educationHistory
     * @param Portfolio $portfolio
     * @return Response
     */
    public function edit($id, EducationHistory $educationHistory, Portfolio $portfolio)
    {
        return view('admin.educationHistories.edit')->with('educationHistory', $educationHistory->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, CreateEducationHistoryRequest $request, EducationHistory $education)
    {
        $education->find($id)->update($request->all());
        Session::flash('message', 'Your Education history was successfully edited!.');
        Session::flash('flash_type', 'alert-success');

        return redirect('education');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllEducation(EducationHistory $educationHistory)
    {
        return \response::json($educationHistory->all());
    }
}
