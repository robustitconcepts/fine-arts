<?php namespace App\Http\Controllers;

use App\Repository\ArticleRepository as Article;
use App\Repository\SliderRepository as Slider;

use App\Http\Requests\ContactFormRequest;
use Illuminate\Support\Facades\Mail;
use App\Repository\MenuRepository as Menu;

/**
 * Class FrontEndController
 * @package App\Http\Controllers
 */
class FrontEndController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

    }


    /**
     * @param Article $article
     * @param Slider $slider
     * @return string
     */
    public function index(Article $article, Slider $slider)
    {
        $news = $article->getArticlesByPublishedDate(3, 'news');
        $images = $slider->all(array('title', 'image', 'description'));
        $frontPage = true;
        $view = view('frontend.index')->with(compact('news', 'images', 'frontPage'))->render();

        return $view;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getPortfolio()
    {
        return view('frontend.portfolio');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getAboutUs(Article $article)
    {
        $articles = $article->getArticlesByPublishedDate(5);

        return view('frontend.aboutUs', compact('articles'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getContactUs(Article $article)
    {
        $articles = $article->getArticlesByPublishedDate(5);

        return view('frontend.contactUs', compact('articles'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getSingleNews($id)
    {
        return view('frontend.singleNews');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getAllArtists()
    {
        return view('frontend.artistList');
    }

    /**
     * @param Article $article
     * @return \Illuminate\View\View
     */
    public function getNews(Article $article)
    {

        $articles = $article->getArticlesByPublishedDate(3);
        $articles->setPath('');

        return view('frontend.news', compact('articles'));
    }

    /**
     * @param ContactFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function contactUs(ContactFormRequest $request)
    {

        /*$mail->send('emails.contact',
            array(
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'subject' => $request->get('subject'),
                'user_message' => $request->get('message')
            ), function ($message) {
                $message->from('shashi.gharti@gmail.com');
                $message->to('shashi.gharti@gmail.com', 'Admin')->subject("Testing");
            });*/
        return redirect('dynamiccontents/2')->with('message',
            'Thank you for contacting us!! We will get back to you shortly!!');
    }

}
