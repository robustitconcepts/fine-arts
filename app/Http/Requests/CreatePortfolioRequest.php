<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePortfolioRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	   return [
            'first_name'=>'required',
            'last_name'=>'required',
            'art_category_id'=>'required',
            'contact_no'=>'required',
            'description'=>'required'
        
           ];
	}

}
