<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/***************    Front End   ***********************/
Route::get('/', 'FrontEndController@index');
Route::get('/home', 'FrontEndController@index');
Route::get('/portfolio', 'FrontEndController@getPortfolio');
Route::get('/news', 'FrontEndController@getNews');
Route::get('/news/{id}', 'FrontEndController@getSingleNews');
Route::get('/artists', 'FrontEndController@getAllArtists');
Route::get('/dashboard', 'DashboardController@index');

/****************** Ajax Queries (JSON)**************************************/
Route::get('/getCategories', 'ArtCategoryController@getAllArtCategory');
Route::get('/getPhotos', 'PhotoController@getPhotosByCategoryNArtist');
Route::get('/getArtists', 'PortfolioController@getArtistsByCategoryNArtist');
Route::get('/searchArtistByAlphabet', 'PortfolioController@searchArtistByAlphabet');
Route::get('/getSliderImages', 'SliderController@getSliderImages');
Route::get('/getArtistPhotos', 'PhotoController@getArtistPhotos');
Route::get('/getAllArtists', 'PortfolioController@getAllArtist');
Route::post('/contactus', 'FrontEndController@contactUs');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);





Route::group(['middleware' => 'admin'], function () {

    /**************** Slider **************************/
    Route::resource('sliders', 'SliderController');

    /**************** Dynamic Contents **************************/
    Route::resource('dynamiccontents', 'DynamicContentController',['except' => 'show']);

    /**************** Album **************************/
    Route::resource('albums', 'AlbumController');


    /**************** Photos ************************/
    Route::resource('photos', 'PhotoController',['except' => 'show']);
    Route::get('photo-album/{albumID}/{page?}', 'PhotoController@getPhotosByAlbum');


    /*************** Portfolios *******************/
    Route::resource('portfolios', 'PortfolioController');
    Route::post('updateProfile', ['as'=>'updateProfile','uses' =>'PortfolioController@updateProfile'] );



    /*************** ArtCategories ***************/
    Route::resource('artcategories', 'ArtCategoryController');


    /*************** Article ************************/
    Route::resource('articles', 'ArticleController',['except' => 'show']);


    /*************** EducationHistory ************************/
    Route::resource('education', 'EducationHistoryController');


    /*************** Awardhistroy ************************/
    Route::resource('awardhistories', 'AwardHistoryController');

    /*************** Exhibition ************************/
    Route::resource('exhibition', 'ExhibitionController');

    /*************** UserController ************************/
    Route::resource('users', 'UserController', ['except' => 'show']);

    Route::get('userstatus/{id}/{status}', 'UserController@changeStatus');


});
Route::resource('dynamiccontents', 'DynamicContentController',['only' => 'show']);
Route::resource('articles', 'ArticleController',['only' => 'show']);
Route::resource('photos', 'PhotoController', ['only' => 'show']);
Route::resource('users', 'UserController', ['only' => 'show']);
