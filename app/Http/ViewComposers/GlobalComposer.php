<?php namespace app\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Repository\MenuRepository as Menu;
use Illuminate\Contracts\Auth\Guard as Auth;
use App\Repository\UserRepository as User;

class GlobalComposer
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;


    /*
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Auth $auth)
    {

        $this->auth = $auth;
    }

    /*
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if ($this->auth->check()) {
            $activeUser = $this->auth->user();
            $view->with('user', $activeUser);
        }
        $view->with('image_path','public/uploads/images/')->with('thumbnail_path','public/uploads/thumbnail/');
    }
}