<?php namespace app\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Contracts\Auth\Guard as Auth;
use App\Repository\ArtCategory;
use App\Repository\TagRepository as Tag;

class PortfolioComposer
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;


    /*
     * Create a new profile composer.
     *
     * @param  Auth  $auth
     * @return void
     */
    public function __construct(Auth $auth, ArtCategory $artCategory, Tag $tag)
    {

        $this->auth = $auth;
        $this->artCategory = $artCategory;
        $this->tag = $tag;
    }

    /*
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if ($this->auth->check()) {
           $artCategories =  $this->artCategory->getAllCategories();
           $tags = $this->tag->getAllTags();
           $view->with('arts', $artCategories)->with('tags', $tags);
        }


    }
}