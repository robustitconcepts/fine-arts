<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtCategory extends Model {
   protected $guarded = [];
   public function portfolio(){
        return $this->hasMany('App\Portfolio');
    }
    public function photo(){
        return $this->hasMany('App\Photo');
    }
}
