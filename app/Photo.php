<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Photo extends Model {

    protected $guarded = [];

    public function portfolio(){
        return $this->belongsTo('App\Portfolio');
    }
    public function album()    {
        return $this->belongsTo('App\Album');
    }
    public function art_category(){
        return $this->belongsTo('App\ArtCategory');
    }

    /**
     * @param bool $excludeDeleted
     * @return $this
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery($excludeDeleted);
        if(Auth::check() && (!Auth::user()->hasRole('admin')))
            return $query->where('portfolio_id', Auth::user()->profile->id);
        else
            return $query;
    }
}
