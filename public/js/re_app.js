/**
 * Created by shashi on 4/16/15.
 */
'use strict';

var base_path = 'http://localhost:8080/fine-arts/';

angular.module('MessageBoxM', ['ui.bootstrap'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
})
    .controller('MessageBoxCtrl', function ($scope, $http, $window) {
    $scope.basePath = $window.base_path;

    $scope.submitForm = function () {
        $('.contact-frm').submit();
    };
});

angular.module('RelatedArtWorkM', ['ui.bootstrap'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}).controller('RelatedArtWorkCtrl', function ($scope, $http, $window) {
    $scope.basePath = $window.base_path;

    $scope.init = function (id) {
        $scope.url = $scope.basePath + 'getArtistPhotos';
        $scope.id = id;

        $http.get($scope.url, {
            params: {
                artist_id: 1,
                rec_count: 11
            }
        }).success(function (data) {
            $scope.images = data;
        });
    };

    $scope.getImagePath = function (image) {
        return $scope.basePath + "public/uploads/thumbnails/" + image;
    };


});

angular.module('SearchM', ['ui.bootstrap'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}).controller('ArtistViewCtrl', function ($scope, $http, $window) {
    $scope.page = 1;
    $scope.alphabet = "";
    $scope.artist = "";
    $scope.category = "";
    $scope.basePath = $window.base_path;
    $scope.last_page = 0;
    $scope.spinnerClass = "";

    $http.get($scope.basePath + 'getCategories').success(function (data) {
        $scope.categories = data;
    });
    $http.get($scope.basePath + 'getArtists').success(function (data) {
        $scope.artists = data.data;
    });
    $http.get($scope.basePath + 'getAllArtists').success(function (data) {
        $scope.artistLists = data;
    });
    $scope.onKeyPress = function () { // this gets executed when the enter button is pressed
        $scope.category = ($scope.selCategory) ? $scope.selCategory.name : "";
        $scope.artist = ($scope.selArtist) ? $scope.selArtist.first_name : "";
        this.getPhotos();
    };
    $scope.getPhotos = function () {
        $scope.url = $scope.basePath + 'getArtists';

        $scope.spinnerClass = "spinner-class";
        $http.get($scope.url, {
            params: {
                artist_name: $scope.artist,
                category: $scope.category,
                page: $scope.page
            }
        }).success(function (data) {
            $scope.artists = data.data;
            $scope.last_page = data.last_page;
            $scope.total_records = data.total;
        }).error(function (data, status) {
            console.error('Error', status, data);
        }).finally(function () {
            $scope.spinnerClass = "";
        });

    };
    $scope.onPageClick = function (page) {
        $scope.page = page;
        if ($scope.alphabet === "")
            this.getPhotos();
        else
            this.searchArtistByAlphabet($scope.alphabet);
    };
    $scope.searchArtistByAlphabet = function (alphabet) {
        $scope.url = $scope.basePath + 'searchArtistByAlphabet';
        $scope.alphabet = alphabet;

        $http.get($scope.url, {params: {page: $scope.page, alphabet: $scope.alphabet}}).success(function (data) {
            $scope.artists = data.data;
            $scope.last_page = data.last_page;
        });
    };

})
    .controller('PortfolioCtrl', function ($scope, $http, $window) {

        $scope.category = "";
        $scope.artist = "";
        $scope.artistLists = "";
        $scope.spinnerClass = "spinner-class";

        $scope.page = 1;

        $scope.basePath = $window.base_path;


        $http.get($scope.basePath + 'getCategories').success(function (data) {
            $scope.categories = data;
        });
        $http.get($scope.basePath + 'getArtists').success(function (data) {
            $scope.artists = data.data;
        });

        $http.get($scope.basePath + 'getAllArtists').success(function (data) {
            $scope.artistLists = data;
        });

        $http.get($scope.basePath + 'getPhotos').success(function (data) {
            $scope.imageDetails = data;
        }).finally(function(){
            $scope.spinnerClass = "";
        });

        $scope.getImagePath = function (image) {
            return $scope.basePath + "public/uploads/thumbnails/" + image;
        };

        $scope.onKeyPress = function () { // this gets executed when the enter button is pressed
            $scope.category = ($scope.selCategory) ? $scope.selCategory.name : "";
            $scope.artist = ($scope.selArtist) ? $scope.selArtist.first_name : "";
            this.getPhotos();
        };

        $scope.onPageClick = function (page, category, artist) {
            $scope.category = ($scope.selCategory) ? $scope.selCategory.name : "";
            $scope.artist = ($scope.selArtist) ? $scope.selArtist.first_name : "";
            $scope.page = page;
            this.getPhotos();
        };

        $scope.onLinkClicked = function (category) {
            $scope.selCategory = {name: category};
            $scope.selArtist = {first_name: ''};

            $scope.category = $scope.selCategory.name;
            $scope.artist = $scope.selArtist.selArtist;

            $scope.page = 1;
            this.getPhotos();
        };

        $scope.getPhotos = function () {
            $scope.url = $scope.basePath + 'getPhotos';
            $scope.spinnerClass = "spinner-class";
            $http.get($scope.url, {
                params: {
                    artist_name: $scope.artist,
                    category: $scope.category,
                    page: $scope.page
                }
            }).success(function (data) {
                $scope.imageDetails = data;
                $scope.last_page = data.last_page;
                $scope.total_records = data.total;
            }).error(function (data, status) {
                console.error('Error', status, data);
            }).finally(function () {
                $scope.spinnerClass = "";
            });
        }


    })
    /*
     This directive allows us to pass a function in on an enter key to do what we want.
     */
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {

                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .filter('range', function () {
        return function (input, total) {
            total = parseInt(total);
            for (var i = 2; i < total; i++)
                input.push(i);
            return input;
        };
    });
